from time import sleep
import sys
import os
import subprocess

R = 20. # Resistor for current measurement [kOhm]

def getI():
    v0 = os.popen("i2cget -y 1 0x68 0").read()
    v1 = os.popen("i2cget -y 1 0x68 1").read()
    try:
        v = int(v0[2:4]+v1[2:3], 16) * 0.02 # mV
    except:
        v = 0
    return v#/R # uA

def getV():
#v0 = os.popen("i2cget -y 1 0x6a 2").read()
#v1 = os.popen("i2cget -y 1 0x6a 3").read()

    v0 = os.popen("i2cget -y 1 0x68 2").read()
    v1 = os.popen("i2cget -y 1 0x68 3").read()
    try:
        v = int(v0[2:4]+v1[2:3], 16) * 0.025
    except:
        v = 0
    return v

def getTemp():
    return subprocess.check_output("/opt/vc/bin/vcgencmd measure_temp", shell=True).strip()


try:
    while(True):
        print "\rV = " + str(getV()) + "V  \tI = " + str(getI()) + 'uA  \t' + getTemp() + "  ",
        sys.stdout.flush()
        sleep(1)
except KeyboardInterrupt:
    print "\nGoodbye."
