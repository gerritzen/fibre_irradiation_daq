/********************************************************************\

  Name:         scaler.cpp
  Created by:   Lukas Gerritzen

  Contents:     Simple tool that just prints the scalers for channels
                1 & 2 and the trigger scaler to stdout.
  TODO:         Timestamp as first column

\********************************************************************/

#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"
#include <vector>
#include <algorithm>

#ifdef _MSC_VER
#pragma warning(disable:4996)
#else
#   include <unistd.h>
#   include <sys/time.h>
inline void Sleep(useconds_t x)
{
   usleep(x * 1000);
}
#endif

/*------------------------------------------------------------------*/

int main()
{
   int i, j, nBoards;
   DRS *drs;
   DRSBoard *b;

   /* do initial scan */
   drs = new DRS(2432);

   /* exit if no board found */
   nBoards = drs->GetNumberOfBoards();
   if (nBoards == 0) {
      printf("No DRS4 evaluation board found\n");
      return 0;
   }

   /* continue working with first board only */
   b = drs->GetBoard(0);

   /* initialize board */
   b->Init();

   /* set sampling frequency */
   b->SetFrequency(5, true);

   /* enable transparent mode needed for analog trigger */
   b->SetTranspMode(1);

   /* set input range to -0.5V ... +0.5V */
   b->SetInputRange(0);

   /* use following line to set range to 0..1V */
   //b->SetInputRange(0.5);
   
   Sleep(10); // wait until evaluation board is ready
   //b->SetChannelConfig(0, 8, 4);
   //b->SetChannelConfig(0, 8, 8);
   
   /* use following line to turn on the internal 100 MHz clock connected to all channels  */
   //b->EnableTcal(1);

   /* use following lines to enable hardware trigger on CH1 at 50 mV positive edge */
   if (b->GetBoardType() >= 8) {        // Evaluaiton Board V4&5
      b->EnableTrigger(1, 0);           // enable hardware trigger
      b->SetTriggerSource(1<<8 | 1<<9);        // set CH1 as source
   } else if (b->GetBoardType() == 7) { // Evaluation Board V3
      b->EnableTrigger(0, 1);           // lemo off, analog trigger on
      b->SetTriggerSource(1);           // use CH1 as source
   }
   b->SetTriggerLevel(-0.05);            // -0.05 V
   b->SetTriggerPolarity(true);        // negative edge

   /* use following lines to set individual trigger elvels */
   //b->SetIndividualTriggerLevel(1, 0.1);
   //b->SetIndividualTriggerLevel(2, 0.2);
   //b->SetIndividualTriggerLevel(3, 0.3);
   //b->SetIndividualTriggerLevel(4, 0.4);
   //b->SetTriggerSource(15);
   
   b->SetTriggerDelayNs(100);             // zero ns trigger delay
   
   /* use following lines to enable the external trigger */
   //if (b->GetBoardType() >= 8) {        // Evaluaiton Board V4&5
   //   b->EnableTrigger(1, 0);           // enable hardware trigger
   //   b->SetTriggerConfig(1<<4);        // set external trigger as source
   //} else {                             // Evaluation Board V3
   //   b->EnableTrigger(1, 0);           // lemo on, analog trigger off
   //}


   /////////////


   std::vector<int> rates0;
   std::vector<int> rates1;
   std::vector<int> rates4;

   for (int asd = 0; asd < 5; asd++) {
       Sleep(100);
       int rate0 = b->GetScaler(0);
       int rate1 = b->GetScaler(1);
       int rate4 = b->GetScaler(4);
       rates0.push_back(rate0);
       rates1.push_back(rate1);
       rates4.push_back(rate4);
   }
   int n = rates0.size() / 2;
   std::nth_element(rates0.begin(), rates0.begin() + n, rates0.end());
   std::nth_element(rates1.begin(), rates1.begin() + n, rates1.end());
   std::nth_element(rates4.begin(), rates4.begin() + n, rates4.end());
   printf("%8d\t%8d\t%8d\n",  rates0[n], rates1[n], rates4[n]);

   delete drs;
}
