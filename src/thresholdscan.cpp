/********************************************************************\

  Name:         drs_exam_2048.cpp
  Created by:   Stefan Ritt

  Contents:     Simple example application to read out a DRS4
                evaluation board in 2048-bin configuration (which
                needs a hardware modification on the board)

  $Id: drs_exam.cpp 21308 2014-04-11 14:50:16Z ritt $

\********************************************************************/

#include <math.h>

#ifdef _MSC_VER

#include <windows.h>

#elif defined(OS_LINUX)

#define O_BINARY 0

#include <unistd.h>
#include <ctype.h>
#include <sys/ioctl.h>
#include <errno.h>

#define DIR_SEPARATOR '/'

#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "strlcpy.h"
#include "DRS.h"
#include <vector>
#include <algorithm>

#ifdef _MSC_VER
#pragma warning(disable:4996)
#else
#   include <unistd.h>
#   include <sys/time.h>
inline void Sleep(useconds_t x)
{
   usleep(x * 1000);
}
#endif

/*------------------------------------------------------------------*/

int main()
{
   system("figlet \"Thresholdscan\"");
   int i, j, nBoards;
   DRS *drs;
   DRSBoard *b;
   FILE  *f;

   /* do initial scan */
   drs = new DRS();

   /* show any found board(s) */
   for (i=0 ; i<drs->GetNumberOfBoards() ; i++) {
      b = drs->GetBoard(i);
      printf("Found DRS4 evaluation board, serial #%d, firmware revision %d\n", 
         b->GetBoardSerialNumber(), b->GetFirmwareVersion());
   }

   /* exit if no board found */
   nBoards = drs->GetNumberOfBoards();
   if (nBoards == 0) {
      printf("No DRS4 evaluation board found\n");
      return 0;
   }

   /* continue working with first board only */
   b = drs->GetBoard(0);

   /* initialize board */
   b->Init();

   /* set sampling frequency */
   b->SetFrequency(5, true);

   /* enable transparent mode needed for analog trigger */
   b->SetTranspMode(1);

   /* set input range to -0.5V ... +0.5V */
   b->SetInputRange(0);

   /* use following line to set range to 0..1V */
   //b->SetInputRange(0.5);
   
   /* set 2048-bin mode */
   if (!b->Is2048ModeCapable()) {
      printf("Board is not configured for 2048-bin mode\n");
      return 0;
   }
   Sleep(10); // wait until evaluation board is ready
   //b->SetChannelConfig(0, 8, 4);
   b->SetChannelConfig(0, 8, 8);
   
   /* use following line to turn on the internal 100 MHz clock connected to all channels  */
   //b->EnableTcal(1);

   /* use following lines to enable hardware trigger on CH1 at 50 mV positive edge */
   if (b->GetBoardType() >= 8) {        // Evaluaiton Board V4&5
      b->EnableTrigger(1, 0);           // enable hardware trigger
      b->SetTriggerSource(1<<0);        // set CH1 as source
   } else if (b->GetBoardType() == 7) { // Evaluation Board V3
      b->EnableTrigger(0, 1);           // lemo off, analog trigger on
      b->SetTriggerSource(1);           // use CH1 as source
   }
   b->SetTriggerLevel(-0.07);            // -0.05 V
   b->SetTriggerPolarity(false);        // positive edge

   /* use following lines to set individual trigger elvels */
   //b->SetIndividualTriggerLevel(1, 0.1);
   //b->SetIndividualTriggerLevel(2, 0.2);
   //b->SetIndividualTriggerLevel(3, 0.3);
   //b->SetIndividualTriggerLevel(4, 0.4);
   //b->SetTriggerSource(15);
   
   b->SetTriggerDelayNs(100);             // zero ns trigger delay
   
   /* use following lines to enable the external trigger */
   //if (b->GetBoardType() >= 8) {        // Evaluaiton Board V4&5
   //   b->EnableTrigger(1, 0);           // enable hardware trigger
   //   b->SetTriggerConfig(1<<4);        // set external trigger as source
   //} else {                             // Evaluation Board V3
   //   b->EnableTrigger(1, 0);           // lemo on, analog trigger off
   //}


   /////////////

   time_t rawtime;
   struct tm * timeinfo;
   char filename[80];

   time (&rawtime);
   timeinfo = localtime(&rawtime);

   strftime(filename, 80, "fibre_%F_%T.dat", timeinfo);

   f = fopen(filename, "w");

   for (j=150; j > 0 ; j--) {
       int barWidth = 70;
       float progress = 1 - j / 150.;

       printf("[");
       int pos = barWidth * progress;
       for (int i = 0; i < barWidth; ++i) {
           if (i < pos) printf("=");
           else if (i == pos) printf(">");
           else printf(" ");
       }
       printf("] %d \%    \r", int(progress * 100));
       fflush(stdout);

       b->SetTriggerLevel(-0.001 * j);

       fprintf(f, "%7.3f\t", 0.001*j);
       std::vector<int> rates;

       for (int asd = 0; asd < 5; asd++) {
           Sleep(100);
           int rate = b->GetScaler(0);
           rates.push_back(rate);
       }
       int n = rates.size() / 2;
       std::nth_element(rates.begin(), rates.begin() + n, rates.end());
       fprintf(f, "%7d",  rates[n]);
       fprintf(f, "\n");
   }

   fclose(f);
   
   ///////////////

   
   /* delete DRS object -> close USB connection */
   delete drs;
   printf("%c[2K", 27);
   printf("Thresholdscan written to %s.\n", filename);

}
